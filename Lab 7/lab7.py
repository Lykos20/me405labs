## @file lab7.py
# This file creates a class to scan for the position of the location at which
# a touchscreen is touched.
#
# The four functions for this class are init, xScan, yScan, zScan, readings, 
# and timeAvg
#
# File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%207/lab7.py
# @author Tatum X. Yee and Tyler J. Guffey
# @date 9 March 2021
#

from pyb import Pin 
from pyb import ADC
#import pyb
import pyb, utime

## This class screen will initialize the directional pins of the touchscreen
#  along with the touch screen dimensions and then scan in the x, y, and z 
#  direction for contact. It will also average the amount of time that each
#  scan takes to occur and the average total time it takes all three scans to 
#  occur.
class scanTouchScreen:
    '''
    @brief Initalizes values and then scans for the location on the x and y axis
           Will also show whether the touchscreen has been pressed
    '''
    ## Initialize the Scanner module
    def __init__(self, xp, xm, yp, ym, l, w, c, multx, multy):
        ''' 
        @brief Creates a TouchScreen Object that can read position
        @param xp   xp pin of touchscreen
        @param xm   xm pin of touchscreen
        @param yp   yp pin of touchscreen
        @param ym   ym pin of touchscreen
        @param l    Length of touchscreen
        @param w    Width of touchscreen
        @param c    Center of touchscreen
        @param multx Constant multiplier for x position correction
        @param multy Constant multiplier for y position correction
        '''
        
        ## Pin xp associated with the positive x position on the touchscreen
        self.xp = xp
        ## Pin xm associated with the negative x position on the touchscreen
        self.xm = xm
        ## Pin yp associated with the positive y position on the touchscreen
        self.yp = yp
        ## Pin ym associated with the negative y position on the touchscreen
        self.ym = ym
        ## Length of touch screen
        self.l = l
        ## Width of touch screen
        self.w = w
        ## Center of touch screen
        self.c = c
        ## Multiplier for x position reading
        self.multx = multx
        ## Multiplier for y position reading
        self.multy = multy
        ## Total time it takes the readings to run for a certain number of iterations
        self.time = 0
        ## Puts the x, y, and x positions in a tuple.
        self.touch_pos = (0,0,False)
    
    ## Scans for the X position of the current contact point
    def xScan(self):
        '''
        @brief Scans for the X position of the current contact point.
               Xp pin set high, 
               Xm pin set low, 
               Yp pin set as a floating push pull pin, and 
               Ym pin set as an analog pin that will be measure and then convert
                  into the X position value
        '''
        ## Time the scan in the x direction begins
        start_x = pyb.micros()
        ## The xp pin set as a high push pull pin
        self.xp.init(mode=Pin.OUT_PP, value=1)
        ## The xm pin set as a low push pull pin
        self.xm.init(mode=Pin.OUT_PP, value=0)
        ## The yp pin set as a floating push pull pin
        self.yp.init(mode=Pin.IN)
        ## The ym pin set as an analog pin that will be measured
        self.ym.init(mode=Pin.ANALOG)
        
        ## The ym pin used as an ADC to read the ADC count
        ymADC = ADC(self.ym)
        ## Avoid Nonlinear response by waiting 4 us
        utime.sleep_us(4)
        ## Position of contact on the x axis
        xPos = (ymADC.read()*self.multx) - self.c[0] - 10.3   # My board is offset by ~10mm
        ## Time it took for x reading to complete
        end_x = pyb.elapsed_micros(start_x)
        ## Update the time index
        self.time += end_x
        #print('xScan time : ' + str(end_x))
        # Return the x position obtained from the scan in mm
        return(xPos)
        #print('X distance: ' + str(xPos))
    
    ## Scans for the current y location of the contact point
    def yScan(self):
        '''
        @brief Scans for the Y position of the current contact point.
               Yp pin set high, 
               Ym pin set low, 
               Xp pin set as a floating push pull pin, and 
               Xm pin set as an analog pin that will be measure and then convert
                  into the Y position value
        '''
        ## Time the scan in the y direction begins
        start_y = pyb.micros()
        ## The yp pin set as a high push pull pin
        self.yp.init(mode=Pin.OUT_PP, value=1)
        ## The ym pin set as a low push pull pin
        self.ym.init(mode=Pin.OUT_PP, value=0)
        ## The xp pin set as a floating push pull pin
        self.xp.init(mode=Pin.IN)
        ## The xm pin set as an analog pin that will be measured
        self.xm.init(mode=Pin.ANALOG)
        
        ## The xm pin used as an ADC to read the ADC count
        xmADC = ADC(self.xm)
        ## Avoid Nonlinear response by waiting 4 us
        utime.sleep_us(4)
        ## Position on the y axis
        yPos = -(xmADC.read()*self.multy)+self.c[1] + 6.5
        ## Time it took for the y reading to complete
        end_y = pyb.elapsed_micros(start_y)
        ## Update the time index
        self.time += end_y
        #print('yScan time: ' + str(pyb.elapsed_micros(start_y)))
        ## Return the y position obtained from the scan in mm
        return(yPos)
        #print('Y distance: ' + str(yPos))
    
    ## Scans for the current z location on the screen to determine if someone is
    #  touching it
    def zScan(self):
        '''
        @brief Scans for the current z location on the screen to determine if 
               someone is touching it
               Yp pin set high, 
               Xm pin set low, 
               Xp pin set as a floating push pull pin, and 
               Ym pin set as an analog pin that will be measure and then convert
                  into the Z position value
        '''
        ## Time this command is started at
        start_z = pyb.micros()
        ## The yp pin set as a high push pull pin
        self.yp.init(mode=Pin.OUT_PP, value=1)
        ## The xm pin set as a low push pull pin
        self.xm.init(mode=Pin.OUT_PP, value=0)
        ## The ym pin set as an analog pin that will be measured
        self.ym.init(mode=Pin.ANALOG)
        
        ## The ym pin used as an ADC to read the ADC count
        ymADC = ADC(self.ym)
        ## Avoid Nonlinear response by waiting 4 us
        utime.sleep_us(4)
        ## Reads ADC count on the y axis
        zTouch = ymADC.read()
        ## Time it took for the z reading to complete
        end_z = pyb.elapsed_micros(start_z)
        ## Update the time index
        self.time += end_z
        #print('Wazzah ' + str(zTouch))
        
        # In between high and low state, return True
        if zTouch < 4000:
            #print('zScan time: ' + str(end_z))  
            #print('Get off of me!')
            return True
        # In the case of high state, no touch detected, so return False
        else:
            #print('I wish I could feel something...I feel...cold')
            #print('zScan time: ' + str(end_z))
            return False           
        
    ## Take a complete reading in the x, y, and z positions if something is 
    #  touching the screen    
    def readings(self):
        '''
        @brief Takes a complete reading in the X, Y, and Z positions if contact
               has been made with the screen. Each component is scanned and then stored
               in a tuple.

        '''
        if self.zScan() == True:
            x = self.xScan()
            y = self.yScan()
            z = self.zScan()
            self.touch_pos = (x,y,z)
            print(self.touch_pos)
        else:
            print('Please touch the screen.')
    
    ## Take the total time and divide by number of iterations to get average time
    #  for each pass
    def timeAvg(self, iterations):
        '''
        @brief After a certain number of iterations, this method returns the 
               average time it takes for the xScan, yScan, and zScan methods to
               run for one pass
        @param iterations   Number of times this program will run
        '''
        ## Number of times we want to run the readings() method
        self.iterations = iterations
        ## The average time taken per iteration for the code to run
        time_avg = self.time/self.iterations  
        print('Average Total Reading Time: ' + str(time_avg))
            
    # def putEverythingTogetherForOptimization(self):
    #     # SCAN X
    #     start_x = pyb.micros()
    #     self.xp.init(mode=Pin.OUT_PP, value=1)
    #     self.xm.init(mode=Pin.OUT_PP, value=0)
    #     self.yp.init(mode=Pin.IN)
    #     self.ym.init(mode=Pin.ANALOG)     
    #     ymADC = ADC(self.ym)
    #     #utime.sleep_us(4)
    #     xPos = (ymADC.read()*self.multx) - self.c[0] - 10.3   # My board is offset by ~10mm
    #     print('xScan time: ' + str(pyb.elapsed_micros(start_x)))
        
    #     # SCAN Z
    #     start_z = pyb.micros()
    #     self.yp.init(mode=Pin.OUT_PP, value=1)
    #     ymADC = ADC(self.ym)
    #     #utime.sleep_us(4)
    #     zTouch = ymADC.read()
    #     print('zScan time: ' + str(pyb.elapsed_micros(start_z)))
        
    #     if zTouch < 4000:
    #         z_val = True
    #     else:
    #         z_val = False
        
    #     # SCAN Y
    #     start_y = pyb.micros()
    #     self.ym.init(mode=Pin.OUT_PP, value=0)
    #     self.xp.init(mode=Pin.IN)
    #     self.xm.init(mode=Pin.ANALOG)
    #     xmADC = ADC(self.xm)
    #     #utime.sleep_us(4)
    #     yPos = -(xmADC.read()*self.multy)+self.c[1]
    #     print('yScan time: ' + str(pyb.elapsed_micros(start_y)))

        
    #     summary_tuple = (xPos, yPos, z_val)
        
    #     if z_val == True:
    #         print(summary_tuple)
    #     else:
    #         print('Stop playing around')
        
if __name__ == '__main__': 
    # Assign Pins
    ## The xp pin set as a floating push pull pin
    xp = Pin.cpu.A7
    ## The xm pin set as a floating push pull pin
    xm = Pin.cpu.A1
    ## The yp pin set as a floating push pull pin
    yp = Pin.cpu.A0
    ## The ym pin set as a floating push pull pin
    ym = Pin.cpu.A6
    
    ## Length of resistive touch panel in mm
    l = 172
    ## Width of resistive touch panel in mm
    w = 102
    ## Center of resistive touch panel in mm
    c = (l/2, w/2)

    ## Conversion factor for the x component (ADC Count to position)
    multx = l/3600
    ## Conversion factor for the y component (ADC Count to position)
    multy = w/3800
    
    ## Assign the task to run it
    scanDatScreen = scanTouchScreen(xp, xm, yp, ym, l, w, c, multx, multy)
    ## Number of runs for test
    iterations = 100
    
    ## Run it back G
    for i in range(iterations):
        # Return a reading on the screen
        scanDatScreen.readings()
        #scanDatScreen.putEverythingTogetherForOptimization()
        
        # Time between each pass
        utime.sleep(2)
    # Return the average time on the screen
    scanDatScreen.timeAvg(iterations)   

        
        
        
        
    