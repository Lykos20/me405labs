## @file main_lab3.py
#
# This file is the front end of the program that allows the console to communicate
# with the nucleo.
#
# File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%203/main_lab3.py
# @author Tatum Yee

import serial
import keyboard
import numpy
import matplotlib.pyplot as plt
import array

## Initializing serial port to talk to Nucleo
ser = serial.Serial(port='COM3', baudrate=115200, timeout=1)

## Creates an integer array for time values collected from nucleo
time = array.array('i')
## Creates a floating array for voltage values collected from nucleo
voltage = array.array('f')
## Creates a data array that will store the information we collect from nucleo
data = []

while True:
    if (keyboard.is_pressed('g')):
        ser.write(str('G').encode('ascii'))
        print('G has been entered to start the program.\n')
             #'Please press the blue button to begin collecting data.')
    
    ## The data that the nucleo sends back to the PC that is being decoded
    myval = ser.readline().decode('ascii').strip('\r\n').split(',')
    print(myval)
    if myval[0] != '':
        try:
            data.append(myval)
            time.append(int(myval[0]))
            voltage.append(3.3/4095*float(myval[1]))
            if float(myval[0]) == 5000:
                print('Data has been collected')
                break
        except:
            break
        
plt.plot(time, voltage)
plt.title('Voltage of Button Over Time')
plt.xlabel('Time [us]')
plt.ylabel('Voltage [V]')
## Labels the X and Y axis of the graph
header = 'Time [us],Voltage [V]'
## The symbol that seperates data in the .csv file
delimiter = ','
## Formats our data to be readable
fmt='%s'
numpy.savetxt('Button_Data.csv', data, delimiter, header=header, fmt='%s')
ser.close()