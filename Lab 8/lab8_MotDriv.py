## @file lab8_MotDriv.py
#  This file serves as the Motor Driver Class and will set up our Model DRV8847
#  DC Motor. It will also be able to enable, disable, and set the duty cycle of
#  the motor.
#  
#  File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%208/lab8_MotDriv.py
#
#  @author Tatum X. Yee and Tyler J. Guffey
#  @date March 9, 2021


global fault
## Global variable that will detect the fault
fault = False

import pyb
import utime

## This class allows us to utilize our motors for the term project
class MotorDriver:
    '''This class implements a motor driver for the ME405 term project.'''
    
    ## Initializes the several pins, including the enable and motor pins, along
    #  with the motor channels and timer.
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, channel1, channel2, nFAULT_pin):
        '''
        Creates a motor driver by initializing GPIO pins and turning the motor 
        off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1
                            and manipulate direction of the spin of the motor
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2
                            and manipulate direction of the spin of the motor.
        @param timer        Timer object used for PWM generation
        @param channel1     Channel associated with the motor driver's first input pin (IN1_pin)
        @param channel2     Channel associated with the motor driver's second input pin (IN2_pin)
	    @param nFAULT_pin   A pyb.Pin object used for the Fault indicator pin
        '''
        ## The nSLEEP pin enables or disables the motor spin
        self.nSLEEP_pin = pyb.Pin (nSLEEP_pin, pyb.Pin.OUT_PP)
        ## When the nSLEEP pin is low, the motor is in sleep mode
        self.nSLEEP_pin.low()
        
        ## The first pyb.Pin.cpu pin object that can be used to manipulate the direction
        #  of the spin of the motor depending on whether it is set on high or low
        self.IN1_pin = IN1_pin
        ## The second pyb.Pin.cpu pin object that can be used to manipulate the direction
        #  of the spin of the motor depending on whether it is set on high or low
        self.IN2_pin = IN2_pin
        
        ## The timer 3 used for sending PWM Pulses to the Motor
        self.timer = timer
        
        ## Channel 1 corresponding to the first motor driver pin
        self.channel1 = channel1
        ## Channel 2 corresponding to the second motor driver pin
        self.channel2 = channel2
        
        ## Initializes the nFAULT pin as a pyb.Pin.cpu
        self.nFAULT_pin = nFAULT_pin
        try:
            ## Creating external interrupt for nFAULT
            self.extint = pyb.ExtInt(self.nFAULT_pin, mode=pyb.ExtInt.IRQ_FALLING, 
    				     pull=pyb.Pin.PULL_UP, callback=self.nFAULT_pin)
        except:
            pass
        
    ## Wakes the motor up so it's no longer in sleep mode and it's ready to be used
    def enable(self):
        '''
        @brief Setting the nSLEEP pin to high will enable the motors to spin
        '''
        self.nSLEEP_pin.high()
        print('Enabling Motor')
    
    ## Turns the motor off and stops it from spinning
    def disable(self):
        '''
        @brief Turns off motors so they no longer spin and set nSLEEP pin to low
               so they motors can not be used
        '''
        self.channel1.pulse_width_percent(0)
        self.channel2.pulse_width_percent(0)
        self.nSLEEP_pin.low()
        print('Disabling Motor')
    
    ## Allows user to control the speed and direction in which the motor spins
    def set_duty(self, duty):
        '''
        This method sets the duty cycle to be sent to the motor to the given level. 
        Positive values cause effort in one direction, negative values in the opposite
        direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to
                    the motor
        '''
        ## The user inputted integer at which they want the motors to spin. A negative
        # integer results in mtors reversing and a positive integer results in the motors
        # spinning forward.
        self.duty = duty
        
        # Positive number = spin forward
        if self.duty > 0:
            self.enable()
            self.channel1.pulse_width_percent(abs(self.duty))
            self.channel2.pulse_width_percent(0)
            print('Spinning forward')
        # Negative number = reverse spin
        elif self.duty < 0:
            self.enable()
            self.channel1.pulse_width_percent(0)
            self.channel2.pulse_width_percent(abs(self.duty))
            print('Spinning backwards')
        # duty = 0 means motor does not runs
        else:
            self.disable()
            print('Motor Stopped')
    ## The method that gets called when the external interrupt occurs. This method
    #  is like an emergency shut off.
    def nFAULT(self,pin):
        '''
        @brief When the external interrupt is triggered, this method will shut down
        the motors
        '''
        global fault
        fault = True
        self.disable()
        print('Yo this current is killing me. Im gonna die.') 
        
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. 
    # Any code within the if __name__ == '__main__' block will only run when 
    # the script is executed as a standalone program. If the script is 
    # imported as a module the code block will not run. Create the pin objects
    # used for interfacing with the motor driver
    
    ## A pyb.Pin object to use as the enable pin.
    pin_nSLEEP = pyb.Pin.cpu.A15
    ## A pyb.Pin object to use as the nFAULT pin to check for current overload
    pin_nFAULT = pyb.Pin.cpu.B2
    
    ## A pyb.Pin object to use as the input to half bridge 1 for motor 1
    pin_IN1 = pyb.Pin.cpu.B4
    ## A pyb.Pin object to use as the input to half bridge 2 for motor 1
    pin_IN2 = pyb.Pin.cpu.B5  
    ## A pyb.Pin object to use as the input to half bridge 1 for motor 2
    pin_IN12 = pyb.Pin.cpu.B0
    ## A pyb.Pin object to use as the input to half bridge 2 for motor 2
    pin_IN22 = pyb.Pin.cpu.B1

    ## Timer for motor driver pin channels 1-4
    tim = pyb.Timer(3)
    
    ## The Channel Setup for IN1
    timch1 = tim.channel(1, pyb.Timer.PWM, pin=pin_IN1)
    ## The Channel Setup for IN2
    timch2 = tim.channel(2, pyb.Timer.PWM, pin=pin_IN2)
    ## The Channel Setup for IN3
    timch3 = tim.channel(3,pyb.Timer.PWM, pin=pin_IN12)
    ## The Channel Setup for IN4
    timch4 = tim.channel(4, pyb.Timer.PWM, pin=pin_IN22)

    ## Create a motor 1 object passing in the pins and timer
    moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, timch1, timch2, pin_nFAULT)
    ## Create a motor 2 object passing in the pins and timer
    moe2 = MotorDriver(pin_nSLEEP, pin_IN12, pin_IN22, tim, timch3, timch4, pin_nFAULT)
    
    
    for i in range (10):
        ## Set the duty cycle of motor 1
        moe1.set_duty(20)
        ## Set the duty cycle of motor 2
        moe2.set_duty(50)
        utime.sleep(1)
        moe1.set_duty(-10)
        moe2.set_duty(-80)
        utime.sleep(1)
        moe1.set_duty(0)
        moe2.set_duty(0)