## @file hw1.py
#  This code will return the correct change for a given purchase from Vendotron
#
#  @package lab1
#  This code will return a tuple representing the coins and bills for change
#  given an integer price and tuple payment.

import math

## Takes in a integer price and tuple payment and returns the change in the 
# fewest denominations as possible
def getChange(price, payment):
    '''
    @brief Computes Change
    @param price     Price of the soda
    @param payment   Payment given by the buyer for the soda
    '''
    payment_cents = (payment[0]*1) + (payment[1]*5) + (payment[2]*10) + \
    (payment[3]*25) + (payment[4]*100) + (payment[5]*500) + (payment[6]*1000) + \
    (payment[7]*2000)


    if price < 0:
        change = payment_cents
        #return(payment)
        print('Sorry, Vendotron is out of order =(')
        print('Your change is: $' + str(format(payment_cents*(10**-2),'.2f')))
        print('Money returned: \n', 
              str(payment[0]) + ' pennies \n',
              str(payment[1]) + ' nickels \n',
              str(payment[2]) + ' dimes \n',
              str(payment[3]) + ' quarters \n',
              str(payment[4]) + ' $1 bill(s) \n',
              str(payment[5]) + ' $5 bills(s) \n',
              str(payment[6]) + ' $10 bill(s) \n',
              str(payment[7]) + ' $20 bill(s)')
        
    elif price < payment_cents:
        change = payment_cents - price
        print('Your change is: $' + str(format(change*(10**-2),'.2f')))

        for idx in range(0, 8):
            if idx == 0:
                twenties = math.trunc(change/2000)
                change -= twenties*2000
            elif idx == 1:
                tens = math.trunc(change/1000)
                change -= tens*1000
            elif idx == 2:
                fives = math.trunc(change/500)
                change -= fives*500
            elif idx == 3:
                ones = math.trunc(change/100)
                change -= ones*100
            elif idx == 4:
                quarters = math.trunc(change/25)
                change -= quarters*25
            elif idx == 5:
                dimes = math.trunc(change/10)
                change -= dimes*10
            elif idx == 6:
                nickels = math.trunc(change/5)
                change -= nickels*5
            elif idx == 7:
                pennies = math.trunc(change/1)
                change -= pennies*1
            
        yoChange = (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)

        print('Money returned: \n', 
              str(yoChange[0]) + ' pennies \n',
              str(yoChange[1]) + ' nickels \n',
              str(yoChange[2]) + ' dimes \n',
              str(yoChange[3]) + ' quarters \n',
              str(yoChange[4]) + ' $1 bill(s) \n',
              str(yoChange[5]) + ' $5 bills(s) \n',
              str(yoChange[6]) + ' $10 bill(s) \n',
              str(yoChange[7]) + ' $20 bill(s)')
        
    else:
        diff = price - payment_cents
        print('Please insert: ' + str(format(diff*(10**-2),'.2f')))
            
if __name__=='__main__':
    
    price = 100000
    payment = (0, 0, 3, 0, 0, 0, 0, 1)
    
    getChange(price, payment)
