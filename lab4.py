# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 13:26:06 2021

@author: Lykos
"""

import pyb
import array

temp = array.array('f')        # Makes an array to collect temperatures

adc = pyb.ADCAll(12, 0x70000)        # Creating ADCAll object with a resolution of 12

adc.read_core_vref()                 # Reads MCU reference voltage 
val = adc.read_core_temp()           # Reads MCU internal temperature
print(val)                           # Prints temperature in degC...Change to return later
temp.append(val)
#temp.append('{:}\r\n'.format(str(val)))
    

'''
with open('TempReadings.csv', 'w') as a_file:
    for line in temp:
        a_file.write('A line: {:}\r\n'.format(line))
        
print('The file has by now automatically been closed.')
'''