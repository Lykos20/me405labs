## @file lab3.py
#
# This file is the back end of the program that collects voltage data from the 
# pressed nucleo button and sends the data back to the front end.
#
# File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%203/lab3.py
# @author Tatum Yee

import pyb
from pyb import UART
import micropython
import array

## Initializes a UART connection
myuart = UART(2)

## Checks for errors in the external interrupt
micropython.alloc_emergency_exception_buf(200)

## Initializes blue push button on nucleo to Pin C13
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)

## This variable represents whether the button has been pressed or not. Until
#  the button is pressed, this value will remain false
test = False

## Recognizes that the button has been pressed after being called by the external
#  interrupt. This function changes the test variable to True which signifies
#  that the button has indeed been pressed.
def onButtonPressFCN(IRQ_src):
    global test
    test = True

## This sets up the external interrupt so that when the button is pressed, the 
#  onButtonPressFCN() is called
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

## Initializes the ADC object that will use PC0 pin on the board.
adc = pyb.ADC(pyb.Pin.board.PC0)
## Create a timer 2 that runs at a frequency of 200000 Hz
tim = pyb.Timer(2, freq=200000)     
## Creates a memory buffer to temporarily store measured ADC values
buf = array.array('i', (0 for index in range (1001)))

## Value of the user input from the main_lab3.py page
val = None
while True:
    if myuart.any() != 0:
        val = myuart.readchar()
    if val == 71:
        if test == True and adc.read() > 5:
            adc.read_timed(buf, tim)
            print('Collecting Data...')
            for i in range(1001):
                myuart.write('{:},{:}\r\n'.format(5*i, buf[i]))
                test = False
            break