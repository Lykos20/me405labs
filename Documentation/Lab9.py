## @file Lab9.py
#
#  This file brings together the different classes we've been working on for the 
#  past few weeks to get our term project working. The classes that are brought
#  together and implemented are the scanTouchScreen class from lab7.py, 
#  the MotorDriver class from lab8_MotDriv.py, and the  Encoder class from lab8_enc.py.
#  The goal of this lab is to define the current orientation of the platform system
#  that continuously updates with new system information and applies changes to the
#  motors so that a ball can be balance on top of the balancing platform.
#
#  File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Term%20Project/Lab9.py
#  @author Tatum X. Yee, Tyler J. Guffey
#  @date 14 March 2021

from lab7 import scanTouchScreen
from lab8_MotDriv import MotorDriver
from lab8_enc import Encoder

from pyb import Pin
from pyb import ADC
import pyb
import utime

## This goal of this class is to attempt to balance a ball on a platform by manipulating
#  controller gain values.
class BalanceBall:
    '''
    @brief      A finite state machine to Balance a Ball on a Platform
    @details    This class implements a finite state machine to control the
                operation of a motor by use of an encoder input and gain.
    '''
    ## Initializes controller gain values, the motors, the encoders, the touchscreen and the nFAULT pin
    def __init__(self, K1, K2, K3, K4, motor1, motor2, encoder1, encoder2, touchscreen, nFAULT):
        '''
        @brief Initializes variables for the BalanceBall class to use.
        @details A few notes; you may want to ensure that at first the motors are set to 0 duty,
                 and that the platform is initially flat so as to correctly initialize the encoders
        @param K1             An object used to modulate motor torque based on the balls velocity
        @param K2             An object used to modulate motor torque based on the platforms angular velocity
        @param K3             An object used to modulate motor torque based on the balls position
        @param K4             An object used to modulate motor torque based on the angle of the platform 
        @param motor1         An object used to interface with one of the motors
        @param motor2         An object used to interface with the other motor
        @param encoder1       An object used to interface with the encoder associated with the first motor
        @param encoder2       An object used to interface with the encoder associated with the other motor
        @param touchscreen    An object used to interface with the touchscreen
        @param nFAULT         An object used to stop the motors in the case of current overload
        '''
        ## The gain accounting for ball velocity
        self.K1 = K1
        ## The gain acounting for the platform's angular velocity
        self.K2 = K2
        ## The gain accounting for the ball position
        self.K3 = K3
        ## The gain accounting for the angle of the platform
        self.K4 = K4
        ## A tuple holding all the gain values
        self.gains = [self.K1, self.K2, self.K3, self.K4]
        ## Initializing the first motor driver class that will be used in this file
        self.motor1 = motor1
        ## Initializing the other motor driver class that will be used in this file
        self.motor2 = motor2
        ## Initializing the encoder class associated with the encoder for the first motor
        self.encoder1 = encoder1
        ## Initializing the encoder class associated with the encoder for the second motor
        self.encoder2 = encoder2
        ## Initializing the touchscreen class to be used in this file
        self.touchscreen = touchscreen
        ## Initializes the nFAULT pin in the case of a current overload
        self.nFAULT = nFAULT
        
        ## Resistance in motor in units of Ohms
        self.R = 2.21         # Ohms
        ## DC Voltage of motor in units of Volts
        self.V_dc = 12        # Volts
        ## Torque constant in Nm/A
        self.k_t = 13.8*1e-3  # mNm/A -> Nm/A
        
        ## Gear ratio between motor and arm
        self.gear_rat = 4
        
        ## Tuple for the last position of the ball on the touchscreen. The first
        #  value represents the X component and the second value repreents the 
        #  Y component
        self.last_pos = (0,0)
        ## Tuple for the current position of the ball on the touchscreen. The first
        #  value represents the X component and the second value repreents the 
        #  Y component
        self.curr_pos = (0,0)
        ## Current X position of the ball on the touchscreen
        self.curr_x = 0
        ## Previous X position of the ball on the touchscreen
        self.last_x = 0
        ## Current Y position of the ball on the touchscreen
        self.curr_y = 0
        ## Previous Y position of the ball on the touchscreen
        self.last_y = 0
        ## Current angle in the X direction of the platform
        self.curr_thx = 0
        ## Previous angle in the X direction of the platform
        self.last_thx = 0
        ## Current angle in the Y direction of the platform
        self.curr_thy = 0
        ## Previous angle in the Y direction of the platform
        self.last_thy = 0
        
        ## Velocity of the ball in the X direction
        self.xdot = 0
        ## Velocity of the ball in the Y direction
        self.ydot = 0
        ## Angular velocity of the platform in the X direction
        self.thxdot = 0
        ## Angular velocity of the platform in the Y direction
        self.thydot = 0
        
        ## Time at which the timer for the ball contact direction begins
        self.start_balltime = utime.ticks_ms()
        ## Time at which the timer for the platform movement begins
        self.start_platformtime = utime.ticks_ms()
        
        ## Initializes the previous platform angle according to the first encoder
        self.encoder1_angle1 = 0
        ## Initializes the current platform angle according to the first encoder
        self.encoder1_angle2 = 0
        ## Initializes the previous platform angle according to the second encoder
        self.encoder2_angle1 = 0
        ## Initializes the current platform angle according to the second encoder
        self.encoder2_angle2 = 0
        
        ## Initializes the external interrupt in the case that pin B2 gets triggerd
        self.extint = pyb.ExtInt(self.nFAULT_pin, mode=pyb.ExtInt.IRQ_FALLING, 
    				     pull=pyb.Pin.PULL_UP, callback=self.nFAULT_pin)
    
    ## Uses the touchscreen module to obtain the position and speed of the ball   
    def ballPosition(self):
        '''
        @brief Obtains the position of the ball by calling the scanTouchScreen class
               from lab7.py. Then this method measures the amount of time it takes to go from 
               one position to the next. This time will be used to compute the
               velocity of the ball.
        '''
        self.last_pos = self.curr_pos 
        #print(self.last_pos)
        self.touchscreen.readings()
        self.curr_x = self.touchscreen.touch_pos[0]
        self.curr_y = self.touchscreen.touch_pos[1]
        self.curr_pos = (self.curr_x, self.curr_y)
        #print('Current Position of Ball: ' + str(self.curr_pos))
        if self.touchscreen.touch_pos[2] == True:
            curr_touchtime = utime.ticks_ms()
            delta_balltime = utime.ticks_diff(curr_touchtime,self.start_balltime)
            #print(self.curr_pos[0] - self.last_pos[0])
            self.xdot = (self.curr_pos[0] - self.last_pos[0])/(delta_balltime/1000)
            self.ydot = (self.curr_pos[1] - self.last_pos[1])/(delta_balltime/1000)
            #print(self.xdot)
            #print(self.ydot)
            self.start_balltime = utime.ticks_ms()
            
    ## Uses the encoder module to obtain the angular position and angular velocity
    #  of the platform.     
    def platformPosition(self):
        '''
        @brief Obtains the angular position of the platform by calling the Encoder
               class from lab8_MotDriv.py. The value returned from the class is then
               adjusted to get an angle in degrees. Then this method measures the
               amount of time it takes to get from one angular position to the next.
               This time will be used to compute the angular velocity of the platform.
        '''
        self.encoder1.get_position()
        curr_platformtime = utime.ticks_ms()
        self.encoder1_angle1 = self.encoder1.previous_pos*360/1100
        self.encoder1_angle2 = self.encoder1.updated_pos*360/1100
        self.encoder2.get_position()
        self.encoder2_angle1 = self.encoder2.previous_pos*360/4000
        self.encoder2_angle2 = self.encoder2.updated_pos*360/4000
        #print('Enc1Ang1: ' + str(self.encoder1_angle1))
        #print('Enc1Ang2: ' + str(self.encoder1_angle2))
        #print('Enc1Ang1: ' + str(self.encoder2_angle1))
        #print('Enc1Ang2: ' + str(self.encoder2_angle2))
        
        ## 60 refers to the lever arm length in mm and 110 refers to the joint
        #  to the pivot of the pushrod in mm
        self.last_thx = self.encoder1_angle1*(60/110)
        self.curr_thx = self.encoder1_angle2*(360/110)*(60/110)
        self.last_thy = self.encoder2_angle1*(60/110)
        self.curr_thy = self.encoder2_angle2*(360/110)*(60/110)
        
        delta_platformtime = utime.ticks_diff(curr_platformtime, self.start_platformtime)
        self.encoder1.get_delta()
        self.encoder2.get_delta()
        angle1 = self.encoder1.delta_pos*(360/1100)*(60/110)
        angle2 = self.encoder2.delta_pos*(360/1100)*(60/110)
        #print('Angle1 start: ' + str(self.encoder1.previous_pos) + ' ' + str(self.encoder1.updated_pos))
        #print('Angle 1: ' + str(angle1))
        #print('Angle 2: ' + str(angle2))
        self.encoder2.get_delta()
        self.thxdot = (angle1)/(delta_platformtime/1000)
        #print(self.thxdot)
        self.thydot = (angle2)/(delta_platformtime/1000)
        #print(self.thydot)
        self.start_platformtime = utime.ticks_ms()
    ## Uses the motor driver module to send duty cycle values to the motor.   
    def rollThatBall(self):
        '''
        @brief Torque is found by manipulating the gain values which is then used
               to get duty cycle % values for PWM. The duty cycle value will continue
               adjusting itself as the ball rolls across the platform. A negative
               duty cycle value indicates that the motor should spin in reverse
               and a positive duty cycle value indicates that the motor should spin
               forwards.
        '''
        self.touchscreen.readings()
        if self.touchscreen.touch_pos[2] == True:
            torque1 = -self.gains[0]*self.xdot - self.gains[1]*self.thxdot - self.gains[2]*self.curr_x - self.gains[3]*self.curr_thx
            duty1 = self.gear_rat*(self.R/(self.V_dc*self.k_t))*torque1
        else:
            duty1 = 0
        self.motor1.set_duty(duty1)
        print('Duty1: ' + str(duty1))
        
        if self.touchscreen.touch_pos[2] == True:
            torque2 = -self.gains[0]*self.ydot - self.gains[1]*self.thydot - self.gains[2]*self.curr_y - self.gains[3]*self.curr_thy
            duty2 = self.gear_rat*(self.R/(self.V_dc*self.k_t))*torque2
        else:
            duty2 = 0
        self.motor2.set_duty(duty2)
        print('Duty2: ' + str(duty2))
        print(' ')
        
    # def plzRoll(self):
    #     self.touchscreen.readings()
    #     self.motor1.enable()
    #     self.motor2.enable()
    #     if self.touchscreen.touch_pos[2] == True:
    #         duty1 = 20
    #     else:
    #         duty1 = 0
    #     self.motor1.set_duty(duty1)
    #     if self.touchscreen.touch_pos[2] == True:
    #         duty2 = 20
    #     else:
    #         duty2 = 0
    #     self.motor1.set_duty(duty2)
    
    ## The method that gets called when the external interrupt occurs. This method
    #  is like an emergency shut off.
    def nFAULT(self, pin):
        '''
        @brief When the external interrupt is triggered, this method will shut down
        the motors
        '''
        self.motor1.nFAULT()
        self.motor2.nFAULT()
        print('Yo this current is killing me. Im gonna die.')
    
if __name__ == '__main__': 
    # Assign Pins
    ## The xp pin set as a floating push pull pin
    xp = Pin.cpu.A7
    ## The xm pin set as a floating push pull pin
    xm = Pin.cpu.A1
    ## The yp pin set as a floating push pull pin
    yp = Pin.cpu.A0
    ## The ym pin set as a floating push pull pin
    ym = Pin.cpu.A6
    
    ## Length of resistive touch panel in mm
    l = 172
    ## Width of resistive touch panel in mm
    w = 102
    ## Center of resistive touch panel in mm
    c = (l/2, w/2)

    ## Conversion factor for the x component (ADC Count to position)
    multx = l/3600
    ## Conversion factor for the y component (ADC Count to position)
    multy = w/3800
        
    ## Assign the task to run it
    scanDatScreen = scanTouchScreen(xp, xm, yp, ym, l, w, c, multx, multy)
    
    
    
    
    
    
    
    
    ## A pyb.Pin object to use as the enable pin.
    pin_nSLEEP = pyb.Pin.cpu.A15
    ## A pyb.Pin object to use as the nFAULT pin to check for current overload
    pin_nFAULT = pyb.Pin.cpu.B2
    
    ## A pyb.Pin object to use as the input to half bridge 1 for motor 1
    pin_IN1 = pyb.Pin.cpu.B4
    ## A pyb.Pin object to use as the input to half bridge 2 for motor 1
    pin_IN2 = pyb.Pin.cpu.B5  
    ## A pyb.Pin object to use as the input to half bridge 1 for motor 2
    pin_IN12 = pyb.Pin.cpu.B0
    ## A pyb.Pin object to use as the input to half bridge 2 for motor 2
    pin_IN22 = pyb.Pin.cpu.B1

    ## Timer for motor driver pin channels 1-4
    tim = pyb.Timer(3)
    
    ## The Channel Setup for IN1
    timch1 = tim.channel(1, pyb.Timer.PWM, pin=pin_IN1)
    ## The Channel Setup for IN2
    timch2 = tim.channel(2, pyb.Timer.PWM, pin=pin_IN2)
    ## The Channel Setup for IN3
    timch3 = tim.channel(3,pyb.Timer.PWM, pin=pin_IN12)
    ## The Channel Setup for IN4
    timch4 = tim.channel(4, pyb.Timer.PWM, pin=pin_IN22)

    ## Create a motor 1 object passing in the pins and timer
    moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, timch1, timch2, pin_nFAULT)
    ## Create a motor 2 object passing in the pins and timer
    moe2 = MotorDriver(pin_nSLEEP, pin_IN12, pin_IN22, tim, timch3, timch4, pin_nFAULT)
    
    
    
    
    
    
    
   
    ## E1 CH1
    pin1 = pyb.Pin(pyb.Pin.cpu.B6)
    ## E1 CH2
    pin2 = pyb.Pin(pyb.Pin.cpu.B7) 
    ## E2 CH1
    pin3 = pyb.Pin(pyb.Pin.cpu.C6) 
    ## E2 CH2
    pin4 = pyb.Pin(pyb.Pin.cpu.C7)  
    
    ## Timer for encoder 1
    tim1 = pyb.Timer(4)
    ## Timer for encoder 2
    tim2 = pyb.Timer(8)
    
    ## Encoder 1, channel 1
    enc1ch1 = tim1.channel(1, pin=pin1, mode=pyb.Timer.ENC_A)
    ## Encoder 1, channel 2
    enc1ch2 = tim1.channel(2, pin=pin2, mode=pyb.Timer.ENC_A)
    ## Encoder 2, channel 1
    enc2ch1 = tim2.channel(1, pin=pin3, mode=pyb.Timer.ENC_B)
    ## Encoder 2, channel 2
    enc2ch2 = tim2.channel(2, pin=pin4, mode=pyb.Timer.ENC_B)
    
    ## Period of encoder 1
    period1 = tim1.init(prescaler=0, period=65535)
    ## Period of encoder 2
    period2 = tim2.init(prescaler=0, period=65535)
    
    ## Encoder 1
    encoder1 = Encoder(pin1, pin2, tim1, enc1ch1, enc1ch2, period1)
    ## Encoder 2
    encoder2 = Encoder(pin3, pin4, tim2, enc2ch1, enc2ch2, period2)
    
    
    
    
    
    
    ## Gain accounting for ball velocity
    K1 = 0.01
    ## Gain acounting for the platform's angular velocity
    K2 = 0.01
    ## gain accounting for the ball position
    K3 = 0.01
    ## gain accounting for the angle of the platform
    K4 = 0.01
    
    ## Creates an object that will be passed into the controlDatBall class
    controlDatBall = BalanceBall(K1, K2, K3, K4, moe1, moe2, encoder1, encoder2, scanDatScreen, pin_nFAULT)
    while True:
        controlDatBall.ballPosition()
        controlDatBall.platformPosition()
        controlDatBall.rollThatBall()
        #controlDatBall.plzRoll()
        utime.sleep(1)
        
    