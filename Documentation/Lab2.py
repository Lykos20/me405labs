## @file lab2.py
#  User reaction time to LED flashing
#
#  A LED turns on randomly between 2-3 seconds of program starting. The user
#  presses the USR button on nucleo as soon as fast as they can. When the program
#  ends, the average time it took for user to press button is returned.
#
#  File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%202/Lab2.py

import pyb
import utime
import random
import micropython


micropython.alloc_emergency_exception_buf(200)
'''
Checks for errors in the external interrupt
'''

## Initializes a number called rand_num that will represent a random number
#  that is generated in the main page.
rand_num = 0

## Initializes the LED we will be using at Pin A5 on the nucleo board.
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## Initializes the user button that we will be using at Pin C13 on the nucleo board.
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)

## Initializes Timer 2 that we will use to count time with a prescaler of 79 and
#  period of 0x7FFFFFFF.
the_timer = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF)
## Initializes the variable at which the counter starts.
start_time = the_timer.counter()

## List for the sum of reaction times of the user. 
sum_reaction_time = 0
## End time at which the user presses the USR button and timer ends
end_time = 0
## Time elapsed since LED turns on to when user hits the button. 
reaction_time = 0
## Counts number of successful user button pushes
counter = 0
## Variable to test whether the external interrupt is engaged (which means the 
#  button has been pressed)
test = False

## Implements function for external interrupt
#  When the USR button is pressed, it interrupts the program
#  and runs this function. This function changes the value of certain indicies
#  in the list of reaction times.
def onButtonPressFCN(IRQ_src):
    global end_time, test
    end_time = the_timer.counter()
    test = True
    print('We just had an interrupt')
    
## Sets up the external interrupt that will callback the onButtonPressFCN function
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)


if __name__ == '__main__':
    
    while True: 

        try:
            if test:
                test = False
                reaction_time = end_time - start_time
                if reaction_time > 0:
                    sum_reaction_time += reaction_time
                    counter += 1
                print('Reaction Time: ' + str((reaction_time)*1e-6) + ' seconds')
                print('Total Time: ' + str((sum_reaction_time)*1e-6) + ' seconds')
                #print('Counter: ' + str(counter))

            for x in range(1):
                rand_num = random.random()
            
            ## Initializes the variable that represents the amount of time
            #  that the LED will remain off before it turns on for one sec.
            delay = 2 + rand_num
            #print(delay)
            pinA5.low()
            #print('low')
            utime.sleep(delay)
            
            pinA5.high()
            start_time = the_timer.counter()
            #print('high')
            utime.sleep(1)

        except KeyboardInterrupt:
            break
    ## Variable that takes the average response time per successful button push
    average = sum_reaction_time / counter
    print('Control-C has been pressed. Here is your average time per try: ' + str(average*1e-6) + ' seconds')
    
