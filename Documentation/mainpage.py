## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#  @section sec_intro0 Introduction
#  The purpose of this portfolio is to highlight all the projects worked on in
#  the Winter 2021 ME 405 Mechatronics course. Below you will find the Table of Contents listing 
#  all of the labs and homeworks that were assigned.
#  
#  @section sec_ToC Table of Contents:
#  Lab 1: Reintroduction to Python and PSMs  
#       
#  Lab 2: Reaction Time Measurement w/ ISR 
#         
#  Lab 3: Pushing the Right Buttons
#    
#  Lab 4: Hot or Not?   
#
#  Lab 5: Feeling Tipsy?
#     
#  Lab 6: Simulation or Reality
# 
#  Lab 7: Feeling Touchy
#
#  Lab 8: Term Project Part 1
#  
#  Lab 9: Term Project Part 2   
#
#  @image html meme1.jpg width=50%
#    
#  @page page_lab1 Lab 1: Reintroduction to Python and PSM
#  @section sec_intro1 What's it do?
#  On start up, the program is in state 0 and the vendotron displays a 
#  welcome page and informs users of the keys they can use to either select a 
#  drink, insert money, or eject money. It then moves on to state 1 where the
#  user can input money, select a drink, or eject using specific predetermined 
#  keys. If a drink is selected, it moves on to stage 2 where a beverage is ejected
#  and change is given if the user input enough money. If not, the user has a 
#  chance to put more money in or eject. If the user ejects, the program moves
#  on to state 3, where all the original coins and bill inserted are given back.
#  @section sec_rev1 Relevant Files
#  lab1.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%201/lab1.py
#  @image html vending_machine.gif
#
#  @page page_lab2 Lab 2: Reaction Time Measurement w/ ISR
#  @section sec_intro2 What's it do?
#  The purpose of this lab is to measure a user's reaction time to an LED turning on.
#  When the program starts running, the LED is turned off. After a time between
#  2 and 3 seconds, the LED will turn on for one second. As soon as the user 
#  sees the LED light up, they will press the blue USR button on the nucleo. 
#  The program will determine the amount of time between when the LED first 
#  lights up to when the user presses the button. This repeats until the user 
#  wants to quit the program with control+c and the program ends with a print 
#  statement telling the user their average reaction time for all successful 
#  attempts. 
#  @section sec_rev2 Relevant Files
#  lab2.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%202/Lab2.py
#  @image html sloth.gif
#  
#  @page page_lab3 Lab 3: Pushing the Right Buttons
#  @section sec_intro3 What's it do?
#  The purpose of this lab is to be able to use the Spyder Console to interface
#  with the nucleo. We will then use this interface to measure the how the voltage 
#  of a pressed button on the nucleo changes over time and how long it takes to
#  to it's fully pressed state. We will visualize this by plotting it on Spyder.
#  When the program starts running, the user needs to input the character G. In
#  ASCII, this value is 71 and the nucleo side will check for this value. Once
#  this value is confirmed, the user will press the blue button on the nucleo.
#  This will trigger the external interrupt which will let the nucleo know that
#  the button has been pressed and that it can start collecting data. After the
#  data has been collected (for one button click duration), the data is returned
#  to the PC side and the data gets plotted.
#  @section sec_prob3 Problems
#  Unfortunately I ran into issues when testing my code and after successfully
#  plotting data once, I didn't have the sense to save it. I then altered my code
#  to add and change other stuff and I never got it to work properly again. I had
#  planned to spend time fixing it after it was already due but then other school
#  work caught up to me and now I'm out of time. However, as I show in my drawing 
#  below, I know that the plot should look something like this. The plot starts 
#  at 0V since it hasn't been touched yet. As soon as the button is pushed, the 
#  voltage increases rapidly at first and then evens out like a first order system.
#  It takes about one millisecond if I remember correctly from that one successful 
#  plot I got and I'm assuming it goes up to around 3.3 volts. Nucleos usually
#  operate the LEDs, buttons, etc with either 3V3 or 5V and I'm guessing a button
#  probably doesn't require 5V. Obviously my drawing can not replace the actual
#  data obtained by my Spyder console but I'm hoping my understanding of how the
#  plot will behave will be of some benefit.
#  @image html lab3plot.PNG 
#  @section sec_rev3 Relevant Files
#  main_lab3.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%203/main_lab3.py         
# 
#  lab3.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%203/lab3.py
#  
#  @page page_lab4 Lab 4: Hot or Not?
#  @section sec_intro4 What's it do?
#  The purpose of this lab is to measure the nucleo board internal temperature,
#  and ambient temperature using a MCP9808 Temperature Sensor Breakout Board and 
#  then plot these two temperatures over time. To start the lab, the MCP9808 pins
#  needed to be soldered on and then wired corectly to the nucleo. For our board,
#  Vcc = 3v3. Below is an image of the MCP9808 Temp Sensor connected to the Nucleo
#  with a solderless breadboard.
#  @image html lab4_wiring.jpg "MCP9808-Nucleo Wiring" width=50%
#  Below is a sample plot of temperature data over 8 hours. On the x-axis, there
#  is time in units of hours and on the y-axis, there is temperature in units of
#  deg Celsius. As noted in the legend, the blue line represents the change of
#  the Nucleo's internal temperature while the orange line represents the change of
#  ambient temperature as measured by the MCP9808 digital temperature sensor.
#  Data collection started at 12:30 am on Monday, February 15, 2021 and ended
#  at 8:30am of the same day. Furthmore, data collection occurred in an apartment
#  in San Luis Obispo, California, United States of America, Earth, Sagittarius Arm, Milky Way, Universe.
#  @image html 8hr_plot.png "Change in Internal Nucleo and Ambient Temperatures over Time" width=50%
#  @section sec_rev4 Relevant Files
#  class_file.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/class_file.py         
# 
#  mcp_file.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/mcp_file.py                        
# 
#  main_file.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/main_file.py       
# 
#  plots.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/plots.py           
# 
#  TempData.csv  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/TempData.csv       
#
#  @page page_lab5 Lab 5: Feeling Tipsy?
#  @section sec_intro5 What's it do?
#  In this lab, we will be analyzing, mathematically, the motions of a pivoting
#  platform. This is in preparation for our Term Project in which we will balance
#  a ball on top of a balancing platform.
#  @section sec_background5 Background Schematics   
#  @image html platform1.PNG 
#  @image html platform2.PNG "Platform Paramaters" 
#  @image html lever_arm.PNG "Lever Arm and Push-Rod Attachment Parameters" width=50%
#  @section sec_analysis5 Analysis
#  @image html lab5pg1.PNG
#  @image html lab5pg2.PNG
#  @image html lab5pg3.PNG
#  @image html lab5pg4.PNG
#  @section sec_rev5 Relevant Files
#  Lab0x05 Take Two 2.pdf  |  https://bitbucket.org/tjguffey/me-405/src/master/Lab0x05/Lab0x05%20Take%20Two%202.pdf 
#
#  @page page_lab6 Lab 6: Simulation or Reality?
#  @section sec_intro6 What's it do?
#  The purpose of this lab is to simulate and test a controller for the pivoting 
#  platform that was analyzed in Lab 5 to show the efficacy of our simplified
#  dynamic model. Throughout this lab, we will use the following numerical parameters
#  in our simulation.
#  @image html lab6_param.PNG width=50%
#  @section sec_results Results
#  We utilized the MATLAB software to model our simulations. First, we initialized
#  all the important parameters using the parameters from the table above. We then
#  linearized our results from Lab 5. After everything was set up, we started 
#  our open-loop simulations and plotted the dynamic response of the velocity, 
#  angular velocity, position, and angle for several different cases.
#
#  Case A, shown in Figure 1, simulates when the ball is initially at rest at the
#  center of gravity of the balancing platform. No torque input is present from 
#  the motor and this simulation is run for 1 second. Our plot shows that all four
#  lines representing the state variables overlap each other at Response = 0. Our
#  results are accurate because we would expect that there is no response initially
#  when everything is at a standstill.
#  @image html lab6plot1.PNG
#  Case B, shown in Figure 2, simulates when a ball is initially placed at rest, 
#  but this time offset horizontally from the center of gravity on the platform
#  by 5cm. The simulation will run for 0.4sec and again, we do not have a torque 
#  input from the motor. Our plot shows that all state variables have varying
#  degrees in an exponential increase of response. Our results are accurate because
#  after the ball is placed offset from the gravitational center, the weight of the
#  ball will create a torque on the balancing platform which will make the platform
#  tilt. The ball will roll down this tilt and increase in position, velocity, angle,
#  and angular velocity as it continues to roll.
#  @image html lab6plot2.PNG
#  Case C, shown in Figure 3, simulates when the ball is initially at rest at the
#  center of gravity of the balancing platform. However, in this case, the platform
#  is tilted by 5 degrees. Like the previous case, there is no torque input from
#  the motor and this simulation will run for 0.4 seconds. The response for case
#  C is very similar to that of Case B. Because the platform starts off tilted,
#  the ball will start to roll and increase in position, velocity, angle, and angular
#  velocity as it continues to roll.
#  @image html lab6plot3.PNG
#  Case D, shown in Figure 4, simulates when the ball is initially at rest at the
#  center of gravity of a flat balancing platform. This time, however, there is
#  an impulse of 1-mNs applied by the motor. This simulation will also be run for
#  0.4 seconds. Our results are accurate because a positive impulse results in
#  a negative response. All of our state variables start at 0 and decrease to 
#  negative values as the platform rotates in a way that causes the ball to roll
#  in the negative direction.
#  @image html lab6plot4.PNG
#  After testing the four open-loop cases above, we moved on to test our simulation
#  in a closed loop with a full state feedback. The gains given to us to test our
#  system performance are K = [-0.05 -0.02 -0.3 -0.2]. The units for these K values
#  are [N-s], [N-m-s], [N], and [N-m]. Although these gains will minimize the
#  amount of actuation effort for stability purposes, they do not offer high performance.
#
#  The closed-loop system simulation, shown in Figure 5, will use the same initial
#  conditions from Case B. To reiterate, the ball is initially placed at rest,
#  offset by 5cm horizontally from the center of gravity on the platform and there
#  is no torque input from the motor. Although it seems that the response oscillated
#  the way it should have, our plot unfortunately seems to get more unstable as
#  it goes on, instead of going to zero.
#  @image html lab6plot5.PNG
#  @section sec_rev6 Relevant Files
#  Please see the following PDF for the lab report, complete with the MATLAB code. 
#             
#  Lab.pdf  |  https://bitbucket.org/tjguffey/me-405/src/master/Lab0x06/Lab.pdf   
#
#  @page page_lab7 Lab 7: Feeling Touchy
#  @section sec_intro7 What's it do?
#  This lab introduces us to the applications of a touchscreen and how to track
#  touches on the x/y/z planes. The X and Y components return a value that represents
#  the distance along the X and Y axis when there is contact with the screen. The
#  Z component returns a boolean value (True or False) depending on whether or 
#  not contact has been made with the screen. These scans are done by energizing
#  the resistor divider between the positive and negative axis pins. The settling
#  period and filter will also be taken into consideration to ensure that we get
#  good readings.
#  @image html lab7hardware.PNG width=90%
#  @section sec_hardware7 Hardware
#  Figure 1 shows our hardware set up. The resistive touch panel was mounted,
#  with tape, to the balancing platform and the flexible printed circuitry (FPC) 
#  breakout board was soldered to the same side of that platform. The FPC cable 
#  connects the touch panel to the FPC adapter, as shown in Figure 2 and then a
#  four pin header wire harness connnects the FPC adapter to the bench-top kit, 
#  as shown in Figure 3. The pinouts for the connectors are shown below in Table 1.
#  @image html pinoutTable.PNG width=40%
#  @section sec_software7 Software
#  The class that scans the touch screen consists of six methods. The first method
#  initializes all the variables that will be used, including the positive and
#  negative axis pins and the touch screen dimensions. The next two methods scan
#  for the X and Y components such that the center of the board reads X=0 and
#  Y=0. Touches to the left of the x axis or below the y axis will result in
#  negative values while touches to the right of the x axis or above the y axis 
#  will result in positive values. The fourth method that scans for the Z component
#  alerts the user to whether board is registering contact or not. The fifth 
#  method returns a tuple of the results given to us by all three component scans 
#  and the last method averages the time it takes for each reading to occur. 
#  Each reading for a single channel must take no longer than 500us and all three 
#  channels must take no longer than 1500us. Furthermore, because the signal 
#  after a scan takes around 3.6us to settle, a delay of 4us occurs before a 
#  measurement is read.
#  @section sec_testing7 Test Procedures
#  @image html TouchScreen.jpeg "Figure 4: Resistive Touch Panel" width=25%
#  The first issue we ran into was making the center of our touchscreen read (0,0) 
#  at the center. I soon realized that, as shown in Figure 4, the active area 
#  of my board did not match that of this part's datasheet. The red box represents
#  my touchscreen's active area and the pink crosshairs show the center of my 
#  active space. You can see that it is slightly offset from the center of the 
#  actual dimensions of the screen. I accounted for this shift in my program when
#  I converted the ADC counts to positions. I found that the range of ADC counts
#  in the X direction was around 200 to 3800 and in the Y direction, around
#  3690-460.
#         
#  Another challenge was optimizing the code such that each scan of a component
#  took no longer than 500us. After trouble shooting and making my program more
#  concise, my results are shown below in Figure 5. The first zScan time records
#  the time it takes to check whether the touchpanel detects contact. If it doesn't,
#  the code does not continue to scan the X and Y components. In this case test,
#  it detected a touch so it scanned and recorded the time it took for the X and
#  Y components. The second zScan is the time it takes for the zScan to happen
#  and return a boolean True/False value. The fifth line displays a tuple of the
#  positions of the X component, the Y component and whether the screen has been
#  touched. The last line is an averaging of the total time it took for each
#  iteration, which included the four scans. These test results show that my code
#  has successfully been optimized and that individually, each scan took less than
#  500us and altogether, less than 1500us.
#  @image html AverageTime.PNG "Figure 5: Test Results" width=25%
#  @section sec_rev7 Relevant Files
#  lab7.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%207/lab7.py        
#
#  @page page_lab8 Lab 8: Term Project Part 1
#  @section sec_intro8 What's it do?
#  The purpose of this lab is to create a motor and encoder driver and begin to
#  create the closed-loop system that was modeled in Lab 6. 
#  @image html motor.jpeg width=25%
#  The motor driver class consists of all the methods needed to run a DC motor 
#  which is connected to the DRV8847 motor driver. Following the table below,
#  we were able to configure our motors to run forwards and backwards.
#  @image html motorpins.PNG width=50%
#  We also initialized our pins following the table below to connect between the
#  CPU, motor driver, and motor.
#  @image html motorconnections.PNG width=30%
#  The motor driver class conssits of six methods. The first initializes the pins
#  mentioned in the second table above and sets up the nFAULT_Pin. The next method
#  enables both motors to run at a predetermined duty cycle and the one following
#  does the opposite. It disables the motors and causes it to come to a stop.
#  The fourth method allows the motors to be deactivated quickly while retaining
#  their values and the next one allows the user to set the duty cycle of the PWM
#  signal sensor. In other words, they can control the speed at which the motor
#  runs in both the clockwise and counterclockwise directions. The last method
#  is in case of current overloads and this abruptly shuts down the motors.
#
#  The encoder class measured the angle at which the motor was rotated. We started
#  off by writing some code to ensure that the encoder pins were connected to the 
#  correct CPU pins, timer and channel, following the table below.
#  @image html encoderpin.PNG width=25%
#  In our situation, as the motor rotated, the tick count would go up. Starting
#  at 0, the highest it would go up to was 65536 ticks before overflowing and 
#  going back to 0. The class consisted of several methods, the first of which 
#  initializes all important values. There is also a method that updates the 
#  current position along with tracking the previous position. The following 
#  method allows the user to view the current position of the motor. Another method
#  allows the user to input a value at which they want the tracking to begin 
#  (if it's not the default 0). Finally, there is a method takes the previous 
#  position and the current position and finds the difference between the two 
#  positions. It is in this last method that we also take into consideration the
#  overflowing and underflowing issues.
#
#  In both of these classes, two sets of motors can be run simultaneously. 
#  @section sec_rev8 Relevant Files                                                                               
#  lab8_enc.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%208/lab8_enc.py                   
# 
#  lab8_MotDriv.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%208/lab8_MotDriv.py                 
#
#  @page page_lab9 Lab 9: Term Project Part 2
#  @section sec_intro9 What's it do?
#  The purpose of this lab is to bring together all of the hardware, drivers, and
#  system drivers we have been working on for the past few weeks. The main classes
#  we will utilize are the scanTouchScreen from lab7.py, Encoder from lab8_enc.py, 
#  and MotorDriver from lab8_MotDriv.py and the goal of this project is to get a 
#  ball to balance on top of the balancing platform. Our class BalanceBall consists
#  of six methods. The first method initializes various values including our gain 
#  values, the initial position of the ball, the initial angle of the 
#  platform, motor parameters, and touchscreen parameters. The next method updates
#  the ball's position and utilizes a timer and some math to figure out the ball's
#  velocity. The third method updates the platform's angle and again utilizes a
#  a timer and some math to figure out the platform's angular velocity. Following
#  that is the method to actually run our motors. In this method, we calculate the 
#  torque and utilize the R, Vdc, and Kt values to get a duty value. The last method
#  is our nFAULT method that detects and shuts down in the presence of high current.
#  After putting in a thousand print statements to troubleshoot, we unfortunately
#  came to the realization that Tatum had probably burned out her motors by having 
#  a duty cycle too high and forgetting to write the nFAULT method before testing. 
#  We were, however, able to simulate how the program should have worked in the video linked below. 
#
#  Term Project Video  |  https://bitbucket.org/Lykos20/me405labs/src/master/Term%20Project/IMG_5069.MOV     
#     
#  Yes, we're aware it looks rather sad but with the exception of a few issues,
#  it does what we want it to do. When the touchscreen isn't being touched and the
#  platform is flat, the motors won't be enabled since the enable pin isn't called
#  unless there is contact. This means that the motors can't spin and the duty 
#  cycle is at 0, as it clearly shows. Then I touched the screen and tried to
#  simulate a ball rolling around with my finger. As you can see, the first two
#  lines (which printed the exact same thing--the tuple for the x, y, and z components) 
#  show that the program recognizes the contact and has enabled the pin that allows 
#  the motors to start spinning. We also had the program print out our duty cycles 
#  (which we should have done before connecting our motors...r.i.p) but our print 
#  statements show that when the duty value is positive, the motor spins forward 
#  and when the value is negative, the motor spins in reverse, like it should if we 
#  could test with hardware. (Note: We know our MotorDriver class works this way since we were
#  able to get  our motors spinning in both directions with different duty cycle values before
#  the motors had burned out)
#  @section sec_conc9 Moving Forward
#  With our code pretty much set up and working how we want it to work, the next
#  steps to actually finishing this project is perfecting the k-values and figuring
#  out what is making the duty value so high in some instances. We would also probably need new motors.
#  To prevent further burn out from future testing, we would add limits to the motor
#  driver class in the set_duty method to prevent duty cycle values above a 
#  certain maximum value from running.
#  @section sec_rev9 Relevant Files
#  Lab9.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Term%20Project/Lab9.p
#  
#  lab8_enc.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%208/lab8_enc.py                   
# 
#  lab8_MotDriv.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%208/lab8_MotDriv.py      
#
#  lab7.py  |  https://bitbucket.org/Lykos20/me405labs/src/master/Lab%207/lab7.py            
#
#  Term Project Video  |  https://bitbucket.org/Lykos20/me405labs/src/master/Term%20Project/Term%20Project%20Video.MOV