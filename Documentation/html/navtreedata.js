/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Project Documentation", "index.html", [
    [ "Introduction", "index.html#sec_intro0", null ],
    [ "Table of Contents:", "index.html#sec_ToC", null ],
    [ "Lab 1: Reintroduction to Python and PSM", "page_lab1.html", [
      [ "What's it do?", "page_lab1.html#sec_intro1", null ],
      [ "Relevant Files", "page_lab1.html#sec_rev1", null ]
    ] ],
    [ "Lab 2: Reaction Time Measurement w/ ISR", "page_lab2.html", [
      [ "What's it do?", "page_lab2.html#sec_intro2", null ],
      [ "Relevant Files", "page_lab2.html#sec_rev2", null ]
    ] ],
    [ "Lab 3: Pushing the Right Buttons", "page_lab3.html", [
      [ "What's it do?", "page_lab3.html#sec_intro3", null ],
      [ "Problems", "page_lab3.html#sec_prob3", null ],
      [ "Relevant Files", "page_lab3.html#sec_rev3", null ]
    ] ],
    [ "Lab 4: Hot or Not?", "page_lab4.html", [
      [ "What's it do?", "page_lab4.html#sec_intro4", null ],
      [ "Relevant Files", "page_lab4.html#sec_rev4", null ]
    ] ],
    [ "Lab 5: Feeling Tipsy?", "page_lab5.html", [
      [ "What's it do?", "page_lab5.html#sec_intro5", null ],
      [ "Background Schematics", "page_lab5.html#sec_background5", null ],
      [ "Analysis", "page_lab5.html#sec_analysis5", null ],
      [ "Relevant Files", "page_lab5.html#sec_rev5", null ]
    ] ],
    [ "Lab 6: Simulation or Reality?", "page_lab6.html", [
      [ "What's it do?", "page_lab6.html#sec_intro6", null ],
      [ "Results", "page_lab6.html#sec_results", null ],
      [ "Relevant Files", "page_lab6.html#sec_rev6", null ]
    ] ],
    [ "Lab 7: Feeling Touchy", "page_lab7.html", [
      [ "What's it do?", "page_lab7.html#sec_intro7", null ],
      [ "Hardware", "page_lab7.html#sec_hardware7", null ],
      [ "Software", "page_lab7.html#sec_software7", null ],
      [ "Test Procedures", "page_lab7.html#sec_testing7", null ],
      [ "Relevant Files", "page_lab7.html#sec_rev7", null ]
    ] ],
    [ "Lab 8: Term Project Part 1", "page_lab8.html", [
      [ "What's it do?", "page_lab8.html#sec_intro8", null ],
      [ "Relevant Files", "page_lab8.html#sec_rev8", null ]
    ] ],
    [ "Lab 9: Term Project Part 2", "page_lab9.html", [
      [ "What's it do?", "page_lab9.html#sec_intro9", null ],
      [ "Moving Forward", "page_lab9.html#sec_conc9", null ],
      [ "Relevant Files", "page_lab9.html#sec_rev9", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Lab9_8py.html",
"main__lab3_8py.html#a0645afebe6b95adb1d450853483cd450"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';