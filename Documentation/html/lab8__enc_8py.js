var lab8__enc_8py =
[
    [ "Encoder", "classlab8__enc_1_1Encoder.html", "classlab8__enc_1_1Encoder" ],
    [ "enc1ch1", "lab8__enc_8py.html#a0dc2d12d852e992ad0e0739b372c3773", null ],
    [ "enc1ch2", "lab8__enc_8py.html#aa0d82f75748637a67a30d676db473b92", null ],
    [ "enc2ch1", "lab8__enc_8py.html#a0d7f3794f2f6eb7be71a6a0b151098f9", null ],
    [ "enc2ch2", "lab8__enc_8py.html#ace1ba65471f99206902d3be987429ed9", null ],
    [ "encoder1", "lab8__enc_8py.html#a324cd96190dcdd9cdeefe8e5d8ee37de", null ],
    [ "encoder2", "lab8__enc_8py.html#aa4f8bfc7b69a80d0f33e974564937bcf", null ],
    [ "period1", "lab8__enc_8py.html#a289c2574839d771f7bebe3657f22dd49", null ],
    [ "period2", "lab8__enc_8py.html#ab639f37006706720585828359e3baf16", null ],
    [ "pin1", "lab8__enc_8py.html#a04e396491b49c4e29b3f4f6de3006765", null ],
    [ "pin2", "lab8__enc_8py.html#aede0e4b0d114c0c2551a5dc232085e44", null ],
    [ "pin3", "lab8__enc_8py.html#a2c6650258207c29e3167f89f3afd0cab", null ],
    [ "pin4", "lab8__enc_8py.html#a2d7c7edeb84ca5a69d8f7ae41179752b", null ],
    [ "tim1", "lab8__enc_8py.html#a325691744ceb4bf71d4c172d1810a0cc", null ],
    [ "tim2", "lab8__enc_8py.html#a0573c0416878fa9823a9e1940c0f90cb", null ]
];