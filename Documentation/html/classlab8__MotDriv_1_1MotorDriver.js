var classlab8__MotDriv_1_1MotorDriver =
[
    [ "__init__", "classlab8__MotDriv_1_1MotorDriver.html#a88c06bea7dae115cd27664a85b493871", null ],
    [ "disable", "classlab8__MotDriv_1_1MotorDriver.html#a769b3f3fcde523db0f67b4c54d4e9130", null ],
    [ "enable", "classlab8__MotDriv_1_1MotorDriver.html#a24dd085a9a37f1fe8b5ef498777a1e02", null ],
    [ "nFAULT", "classlab8__MotDriv_1_1MotorDriver.html#a80a8a822ac04ed4b54a72c87815f2245", null ],
    [ "set_duty", "classlab8__MotDriv_1_1MotorDriver.html#ad1b4e204e240b1e6aaebb1ce48457638", null ],
    [ "channel1", "classlab8__MotDriv_1_1MotorDriver.html#af8c90bbe3d1dd72df108440d0d46db3d", null ],
    [ "channel2", "classlab8__MotDriv_1_1MotorDriver.html#ae5d90145c51b8cdf4f6cfae4a4349c46", null ],
    [ "duty", "classlab8__MotDriv_1_1MotorDriver.html#a751b57aeb045c6a2fb6e8111bb77ef34", null ],
    [ "extint", "classlab8__MotDriv_1_1MotorDriver.html#abc216de99d3c3d88e9a09f485839ae25", null ],
    [ "IN1_pin", "classlab8__MotDriv_1_1MotorDriver.html#a69d8c5b6330a279f9c08fc17d27a7b90", null ],
    [ "IN2_pin", "classlab8__MotDriv_1_1MotorDriver.html#a8c5ad11825dc87a17a332e985478502d", null ],
    [ "nFAULT_pin", "classlab8__MotDriv_1_1MotorDriver.html#a9e3ff9d9f6f0e9a0d22ae5b0f54811a0", null ],
    [ "nSLEEP_pin", "classlab8__MotDriv_1_1MotorDriver.html#aa85726c7f33133222e58c24d6f7be037", null ],
    [ "timer", "classlab8__MotDriv_1_1MotorDriver.html#acfa0dbbdeebc607cbda11a54f92afe12", null ]
];