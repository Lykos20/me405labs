var classlab7_1_1scanTouchScreen =
[
    [ "__init__", "classlab7_1_1scanTouchScreen.html#acd9687ef6ad0427185482c118bdc6dbf", null ],
    [ "readings", "classlab7_1_1scanTouchScreen.html#a1705d3d3467b831c6ff984467f5ef125", null ],
    [ "timeAvg", "classlab7_1_1scanTouchScreen.html#aa9102c6ee8afbc310815a8d4189765e5", null ],
    [ "xScan", "classlab7_1_1scanTouchScreen.html#a2b3b789483476274458f51b5af524584", null ],
    [ "yScan", "classlab7_1_1scanTouchScreen.html#af5acc627d7fc310d9b75e9c8bef75a29", null ],
    [ "zScan", "classlab7_1_1scanTouchScreen.html#a7d7eba668b7cba659b92d57896ad0e2e", null ],
    [ "c", "classlab7_1_1scanTouchScreen.html#accb175fd217b27dcf7de072ef966632b", null ],
    [ "iterations", "classlab7_1_1scanTouchScreen.html#acd8cdcce21239b5bab440576f3518f40", null ],
    [ "l", "classlab7_1_1scanTouchScreen.html#a9ad35b0be6fbcd04f042c0746f5259c9", null ],
    [ "multx", "classlab7_1_1scanTouchScreen.html#a78e7b2e4522bf5fb1d8c02cfe42c8c37", null ],
    [ "multy", "classlab7_1_1scanTouchScreen.html#a0192cc5e16e8840c93ffa9ebda4dc316", null ],
    [ "time", "classlab7_1_1scanTouchScreen.html#a5b9eeee75f4f9d94905962ca90b25de6", null ],
    [ "touch_pos", "classlab7_1_1scanTouchScreen.html#a9d4415ce37ebc99e05c480b01487dc21", null ],
    [ "w", "classlab7_1_1scanTouchScreen.html#a6fe6c415ddec99f079c1e49b7f9a317a", null ],
    [ "xm", "classlab7_1_1scanTouchScreen.html#ac491a250e63bff21ff006d5d4ab129af", null ],
    [ "xp", "classlab7_1_1scanTouchScreen.html#a1ec7a0fbbd8302633104f3a6179dd2c3", null ],
    [ "ym", "classlab7_1_1scanTouchScreen.html#a2ecef77eb857f8797c006796c1d22edf", null ],
    [ "yp", "classlab7_1_1scanTouchScreen.html#a99c384c61eba9ba9716490de51657cbe", null ]
];