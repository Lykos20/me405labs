var searchData=
[
  ['lab_201_3a_20reintroduction_20to_20python_20and_20psm_367',['Lab 1: Reintroduction to Python and PSM',['../page_lab1.html',1,'']]],
  ['lab_202_3a_20reaction_20time_20measurement_20w_2f_20isr_368',['Lab 2: Reaction Time Measurement w/ ISR',['../page_lab2.html',1,'']]],
  ['lab_203_3a_20pushing_20the_20right_20buttons_369',['Lab 3: Pushing the Right Buttons',['../page_lab3.html',1,'']]],
  ['lab_204_3a_20hot_20or_20not_3f_370',['Lab 4: Hot or Not?',['../page_lab4.html',1,'']]],
  ['lab_205_3a_20feeling_20tipsy_3f_371',['Lab 5: Feeling Tipsy?',['../page_lab5.html',1,'']]],
  ['lab_206_3a_20simulation_20or_20reality_3f_372',['Lab 6: Simulation or Reality?',['../page_lab6.html',1,'']]],
  ['lab_207_3a_20feeling_20touchy_373',['Lab 7: Feeling Touchy',['../page_lab7.html',1,'']]],
  ['lab_208_3a_20term_20project_20part_201_374',['Lab 8: Term Project Part 1',['../page_lab8.html',1,'']]],
  ['lab_209_3a_20term_20project_20part_202_375',['Lab 9: Term Project Part 2',['../page_lab9.html',1,'']]]
];
