var searchData=
[
  ['i2c_269',['i2c',['../classmcp__file_1_1mcp.html#a22950507dd388b82213dced26e2cc25c',1,'mcp_file.mcp.i2c()'],['../main__file_8py.html#afbcd4d672f223f9b2e51821423ed6732',1,'main_file.i2c()'],['../mcp__file_8py.html#a459a9221bfe8308557e25a74b40f5d7a',1,'mcp_file.i2c()']]],
  ['i2c_5ftemp_5fsensor_270',['i2c_temp_sensor',['../main__file_8py.html#a5da6239a0fd3583e7e53bf7653700fa7',1,'main_file.i2c_temp_sensor()'],['../mcp__file_8py.html#adb7a944e07cfa05a5e88fce1f8958ebb',1,'mcp_file.i2c_temp_sensor()']]],
  ['in1_5fpin_271',['IN1_pin',['../classlab8__MotDriv_1_1MotorDriver.html#a69d8c5b6330a279f9c08fc17d27a7b90',1,'lab8_MotDriv::MotorDriver']]],
  ['in2_5fpin_272',['IN2_pin',['../classlab8__MotDriv_1_1MotorDriver.html#a8c5ad11825dc87a17a332e985478502d',1,'lab8_MotDriv::MotorDriver']]],
  ['iteration_273',['iteration',['../main__file_8py.html#abee6a2a5107974523c4c290af42c147d',1,'main_file']]],
  ['iterations_274',['iterations',['../classlab7_1_1scanTouchScreen.html#acd8cdcce21239b5bab440576f3518f40',1,'lab7.scanTouchScreen.iterations()'],['../lab7_8py.html#ae3fa1a665eaf185cb485b659affa697c',1,'lab7.iterations()']]]
];
