var searchData=
[
  ['c_235',['c',['../classlab7_1_1scanTouchScreen.html#accb175fd217b27dcf7de072ef966632b',1,'lab7.scanTouchScreen.c()'],['../lab7_8py.html#a5b47a41e629938896664a28af0757d15',1,'lab7.c()'],['../Lab9_8py.html#a4c7a95aa8a5f5ce983aa72800a25930c',1,'Lab9.c()']]],
  ['channel1_236',['channel1',['../classlab8__enc_1_1Encoder.html#a258522fd4f6ec22616e59e861244fff7',1,'lab8_enc.Encoder.channel1()'],['../classlab8__MotDriv_1_1MotorDriver.html#af8c90bbe3d1dd72df108440d0d46db3d',1,'lab8_MotDriv.MotorDriver.channel1()']]],
  ['channel2_237',['channel2',['../classlab8__enc_1_1Encoder.html#a1e07253f0880d233e01fa95cfa3a9d81',1,'lab8_enc.Encoder.channel2()'],['../classlab8__MotDriv_1_1MotorDriver.html#ae5d90145c51b8cdf4f6cfae4a4349c46',1,'lab8_MotDriv.MotorDriver.channel2()']]],
  ['collect_5fnucleo_5ftemp_238',['collect_nucleo_temp',['../main__file_8py.html#ae25183614028f3af93606dc16c4a2870',1,'main_file']]],
  ['controldatball_239',['controlDatBall',['../Lab9_8py.html#af4e4690c190a0dd6395cac8b9168163b',1,'Lab9']]],
  ['counter_240',['counter',['../lab2_8py.html#a4e95d67c0bec2a454a34597427dc15c7',1,'lab2']]],
  ['cuke_5fprice_241',['cuke_price',['../lab1_8py.html#ac26a4095d8b665753f8632e0f01b6348',1,'lab1']]],
  ['curr_5fpos_242',['curr_pos',['../classLab9_1_1BalanceBall.html#aa24d78bfe38c7c56a548e2239c84c12a',1,'Lab9::BalanceBall']]],
  ['curr_5fthx_243',['curr_thx',['../classLab9_1_1BalanceBall.html#ada492d683e93e2dbe254c5cf6a086767',1,'Lab9::BalanceBall']]],
  ['curr_5fthy_244',['curr_thy',['../classLab9_1_1BalanceBall.html#a72dae086bb6a32fbb966fb84592f28d5',1,'Lab9::BalanceBall']]],
  ['curr_5fx_245',['curr_x',['../classLab9_1_1BalanceBall.html#ac166e2331ed389f44d456174e6040791',1,'Lab9::BalanceBall']]],
  ['curr_5fy_246',['curr_y',['../classLab9_1_1BalanceBall.html#ac1009c817abba9c7b5612980edda584f',1,'Lab9::BalanceBall']]]
];
