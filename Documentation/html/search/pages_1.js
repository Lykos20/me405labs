var searchData=
[
  ['lab_201_3a_20reintroduction_20to_20python_20and_20psm_208',['Lab 1: Reintroduction to Python and PSM',['../page_lab1.html',1,'']]],
  ['lab_202_3a_20reaction_20time_20measurement_20w_2f_20isr_209',['Lab 2: Reaction Time Measurement w/ ISR',['../page_lab2.html',1,'']]],
  ['lab_203_3a_20incremental_20encoders_210',['Lab 3: Incremental Encoders',['../page_lab3.html',1,'']]],
  ['lab_204_3a_20hot_20or_20not_3f_211',['Lab 4: Hot or Not?',['../page_lab4.html',1,'']]],
  ['lab_205_3a_20feeling_20tipsy_3f_212',['Lab 5: Feeling Tipsy?',['../page_lab5.html',1,'']]],
  ['lab_206_3a_20simulation_20or_20reality_3f_213',['Lab 6: Simulation or Reality?',['../page_lab6.html',1,'']]],
  ['lab_207_3a_20feeling_20touchy_214',['Lab 7: Feeling Touchy',['../page_lab7.html',1,'']]]
];
