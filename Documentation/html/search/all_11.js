var searchData=
[
  ['scandatscreen_137',['scanDatScreen',['../lab7_8py.html#a302c2d16dbbbfa18e2d86b45cd975645',1,'lab7.scanDatScreen()'],['../Lab9_8py.html#aead5e5addc446df05e9641b776e9fe86',1,'Lab9.scanDatScreen()']]],
  ['scantouchscreen_138',['scanTouchScreen',['../classlab7_1_1scanTouchScreen.html',1,'lab7']]],
  ['ser_139',['ser',['../main__lab3_8py.html#a74dac9a556799afcfffce523716acf77',1,'main_lab3']]],
  ['set_5fduty_140',['set_duty',['../classlab8__MotDriv_1_1MotorDriver.html#ad1b4e204e240b1e6aaebb1ce48457638',1,'lab8_MotDriv::MotorDriver']]],
  ['set_5fposition_141',['set_position',['../classlab8__enc_1_1Encoder.html#a498b56ff804e170b2f2ccb9c08ba6695',1,'lab8_enc::Encoder']]],
  ['spryte_5fprice_142',['spryte_price',['../lab1_8py.html#a2d13ceb0e7e2f192f93d6a7a47696f9c',1,'lab1']]],
  ['start_143',['start',['../main__file_8py.html#af6eef0ae7ab6106ddc75bbabf8eb94f7',1,'main_file']]],
  ['start_5fballtime_144',['start_balltime',['../classLab9_1_1BalanceBall.html#a65c07f036e52d044f7a5da993c494f34',1,'Lab9::BalanceBall']]],
  ['start_5fplatformtime_145',['start_platformtime',['../classLab9_1_1BalanceBall.html#a4e61b5d8d204d704947bd486b8dcf8bf',1,'Lab9::BalanceBall']]],
  ['start_5ftime_146',['start_time',['../lab2_8py.html#a09d5f1d6e621bf303d04ee4bca3ddd76',1,'lab2']]],
  ['state_147',['state',['../lab1_8py.html#a40c9aae6659688d7bf23acc6ef902f67',1,'lab1']]],
  ['sum_5freaction_5ftime_148',['sum_reaction_time',['../lab2_8py.html#a6c1d319742f6d0e759f1b85891f6c2d1',1,'lab2']]]
];
