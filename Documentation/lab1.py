## @file lab1.py
#  User buys a soda
#
#  User enters money to buy soda. If they have enough money, soda is vended and
#  change is given. If they didn't input enough money, soda is not vended. If
#  user changes their mind and no longer wants the drink, they can eject their
#  money.
#
#  File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%201/lab1.py

import math
import keyboard

## Initializes the state that the vendotron starts in (ready to take money and
#  and dispense drinks)
state = 0
## Initializes the pushed_key so that it can be used in the getMoney function.
#  The pushed key will either add money or select a drink.
pushed_key = None
## Price of the Cuke soda in cents
cuke_price = 500
## Price of the Popsi soda in cents
popsi_price = 550
## Price of the Spryte soda in cents
spryte_price = 375
## Price of the Pupper soda in cents
pupper_price = 3399
## Initialization of a list that will hold the money that the user inputs. In order
#  from left to right, the placement holder represents the number of pennies, nickels,
#  dimes, quarters, $1 bills, $5 bills, $10 bills, and $20 bills.
payment_list = [0,0,0,0,0,0,0,0]
## Initalizes the amount of money in cents, taken from the payment_list
payment_dollars = 0
    


## Takes in a integer price and tuple payment and returns the change in the 
# fewest denominations as possible
def getChange(price, payment):
    '''
    @brief Computes Change
    @param price     Price of the soda in cents
    @param payment   Payment (in number of coins or bills) given by the buyer for the soda
    '''
    
    ## Converts the payment list into a total payment value in cents
    payment_cents = (payment[0]*1) + (payment[1]*5) + (payment[2]*10) + \
    (payment[3]*25) + (payment[4]*100) + (payment[5]*500) + (payment[6]*1000) + \
    (payment[7]*2000)

    ## If the price of the soda is less than 0, the machine is out of order and
    #  the money is returned.
    if price < 0:
        change = payment_cents
        print('Sorry, Vendotron is out of order =(')
        print('Your change is: $' + str(format(payment_cents*(10**-2),'.2f')))
        print('Money returned: \n', 
              str(payment[0]) + ' pennies \n',
              str(payment[1]) + ' nickels \n',
              str(payment[2]) + ' dimes \n',
              str(payment[3]) + ' quarters \n',
              str(payment[4]) + ' $1 bill(s) \n',
              str(payment[5]) + ' $5 bills(s) \n',
              str(payment[6]) + ' $10 bill(s) \n',
              str(payment[7]) + ' $20 bill(s)')
    ## If the price of the soda is less than the amount of money that the user gives
    #  change is given in the smallest denomination.
    elif price < payment_cents:
        change = payment_cents - price
        print('Your change is: $' + str(format(change*(10**-2),'.2f')))

        for idx in range(0, 8):
            if idx == 0:
                twenties = math.trunc(change/2000)
                change -= twenties*2000
            elif idx == 1:
                tens = math.trunc(change/1000)
                change -= tens*1000
            elif idx == 2:
                fives = math.trunc(change/500)
                change -= fives*500
            elif idx == 3:
                ones = math.trunc(change/100)
                change -= ones*100
            elif idx == 4:
                quarters = math.trunc(change/25)
                change -= quarters*25
            elif idx == 5:
                dimes = math.trunc(change/10)
                change -= dimes*10
            elif idx == 6:
                nickels = math.trunc(change/5)
                change -= nickels*5
            elif idx == 7:
                pennies = math.trunc(change/1)
                change -= pennies*1
            
        yoChange = (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)

        print('Money returned: \n', 
              str(yoChange[0]) + ' pennies \n',
              str(yoChange[1]) + ' nickels \n',
              str(yoChange[2]) + ' dimes \n',
              str(yoChange[3]) + ' quarters \n',
              str(yoChange[4]) + ' $1 bill(s) \n',
              str(yoChange[5]) + ' $5 bills(s) \n',
              str(yoChange[6]) + ' $10 bill(s) \n',
              str(yoChange[7]) + ' $20 bill(s)')
    ## If not enough money is given to pay for a soda, the machine will prompt
    #  the user to enter the remaining difference.
    else: # Price > payment
        diff = price - payment_cents
        print('Please insert: $' + str(format(diff*(10**-2),'.2f')))

## Welcomes the user to the vendotron. Displays the prices of the soda along with
#  the keys that correspond to different coins and bills.
def printWelcome():
    """
    Prints a Vendotron^TM^ welcome message with beverage prices
    """
    
    print('Welcome to Vendotron! Which drink would you like today? Please tap twice. \n'
          '[c] Cuke: $5.00 \n'
          '[p] Popsi: $5.50 \n'
          '[s] Spryte: $3.75 \n'
          '[d] Dr. Pupper: $33.99 \n'
          '[e] Eject')
    print(' \n'
          'To insert money, press the following options: \n'
          '[0] 1 cent \n'
          '[1] 5 cents \n'
          '[2] 10 cents \n'
          '[3] 25 cents \n'
          '[4] 1 dollar \n'
          '[5] 5 dollars \n'
          '[6] 10 dollars \n'
          '[7] 20 dollars \n')    

## Callback that runs when the user pushes a key on the keyboard 
def getMoney(entry):
    
    global pushed_key
    pushed_key = entry.name

keyboard.on_press(getMoney)  

while True:
    '''
    Implements FSM using a while loop and an if statement that will run eternally
    until user presses CTRL-C to break out of the program.
    '''

    if state == 0:
        '''
        Perform state 0 operations
        This state initializes the FSM itself, welcomes the user, and is ready
        to take user input
        '''
        
        ## Welcomes the user
        printWelcome()
        ## Moves on to the next state in which money is accepted
        state = 1        
        
    elif state == 1: 
        '''
        Perform state 1 operations
        Accepts money and adds the coin or bill count to a list.
        '''
        if pushed_key:
            if pushed_key == '0':
                ## One penny is inputted
                payment_list[0] += 1
                ## 1 cent added to total payment
                payment_dollars += 1
            elif pushed_key == '1':
                ## One nickel is inputted
                payment_list[1] += 1
                ## 5 cents added to total payment
                payment_dollars += 5
            elif pushed_key == '2':
                ## One dime is inputted
                payment_list[2] += 1
                ## 10 cents added to total payment
                payment_dollars += 10
            elif pushed_key == '3':
                ## One quarter is inputted
                payment_list[3] += 1
                ## 25 cents added to total payment
                payment_dollars += 25
            elif pushed_key == '4':
                ## One $1 bill is inputted
                payment_list[4] += 1
                ## 100 cents added to total payment
                payment_dollars += 100
            elif pushed_key == '5':
                ## One $5 bill is inputted
                payment_list[5] += 1
                ## 500 cents added to total payment
                payment_dollars += 500
            elif pushed_key == '6':
                ## One $10 bill is inputted
                payment_list[6] += 1
                ## 1000 cents added to total payment
                payment_dollars += 1000
            elif pushed_key == '7':
                ## One $20 bill is inputted
                payment_list[7] += 1
                ## 2000 cents added to total payment
                payment_dollars += 2000
            elif pushed_key == 'c':
                state = 2
            elif pushed_key == 'p':
                state = 2
            elif pushed_key == 's':
                state = 2
            elif pushed_key == 'd':
                state = 2
            elif pushed_key == 'e':
                state = 3
                
            else:
                print('Invalid')
            
            pushed_key = None
            print('Current Balance: $' + str(format(payment_dollars*(10**-2),'.2f')))
    
    elif state == 2:
        '''
        Perform state 2 operations
        If enough money is inputted to buy a drink, the drink is vended and any
        change is returned. If not enough money is inputted to buy a drink,
        user is prompted to insert more money.
        '''
        if pushed_key:
            if pushed_key =='c':
                if cuke_price <= payment_dollars:
                    print('Cuke vended')
                    ## A tuple created that will contain the contents of payment_list
                    payment_tuple = (payment_list)
                    getChange(cuke_price, payment_tuple)
                    payment_dollars -= payment_dollars
                else:
                    print('Cuke not vended. Please insert more money')
                      
            elif pushed_key == 'p':
                if popsi_price <= payment_dollars:
                    print('Popsi vended')
                    payment_tuple = (payment_list)
                    getChange(popsi_price, payment_tuple)
                    payment_dollars -= payment_dollars
                else:
                    print('Popsi not vended. Please insert more money') 
            elif pushed_key == 's':
                if spryte_price <= payment_dollars:
                    print('Spryte vended')
                    payment_tuple = (payment_list)
                    getChange(spryte_price, payment_tuple)
                    payment_dollars -= payment_dollars
                else:
                    print('Spryte not vended. Please insert more money') 
            elif pushed_key == 'd':
                if pupper_price <= payment_dollars:
                    print('Dr. Pupper vended')
                    payment_tuple = (payment_list)
                    getChange(pupper_price, payment_tuple)
                    payment_dollars -= payment_dollars
                else:
                    print('Dr. Pupper not vended. Please insert more money')
                    
            state = 1   
        
       # getChange(price, payment)
    elif state == 3:
        """
        Perform state 3 operations
        Money is given back if machine is out of order.
        """
        payment_tuple = (payment_list)
        print('Your change is: $' + str(format(payment_dollars*(10**-2),'.2f')))
        print('Money returned: \n', 
              str(payment_tuple[0]) + ' pennies \n',
              str(payment_tuple[1]) + ' nickels \n',
              str(payment_tuple[2]) + ' dimes \n',
              str(payment_tuple[3]) + ' quarters \n',
              str(payment_tuple[4]) + ' $1 bill(s) \n',
              str(payment_tuple[5]) + ' $5 bills(s) \n',
              str(payment_tuple[6]) + ' $10 bill(s) \n',
              str(payment_tuple[7]) + ' $20 bill(s)')
        payment_list = [0,0,0,0,0,0,0,0]
        payment_dollars = 0
        payment_tuple = (payment_list)
        
        state = 0       # s3 --> s1
    
    else:
        '''
        This state shouldn't exist!
        '''
        pass
        