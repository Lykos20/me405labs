## @file plots.py
# This file plots the data received from the temperature sensors.
#
# Temperature is recorded in degrees Celsius while time is recorded in milliseconds.
#
# File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/plots.py
# @author Tatum X. Yee and Tyler J. Guffey
# @date 9 February 2021

import matplotlib.pyplot as plt
import numpy as np

## The symbol that divdes the data we want
delimiter = ','
## Loads data into arrays
time, mcu_temp, mcp_temp = np.loadtxt('TempData.csv',
                  unpack = True,
                  delimiter = ',')

## Total time for plotting
plot_time = time/1000/60/60
## Values recorded by the MCU temperature sensor
mcu_temp = mcu_temp
## Describes what is being plotted
label1 = 'Nucleo Internal Temperature'

## 
## Descrives what is being plotted
label2 = 'MCP9808 Measured Ambient Temperature'
plt.plot(plot_time, mcu_temp, label1)
plt.plot(time/1000/60/60, mcp_temp, label2)
plt.legend()

## Names title
plt.title('Temperature Data')

## Names x-axis
plt.xlabel('Time [hr]')

## Names y-axis
plt.ylabel('Temperatures [degC]')

## Plots on graph
plt.show()
