## @file class_file.py
# This file creates a class for the nucleo temperature sensor.
#
# The two functions for this class are init and tempMeas. Init will initialize all
# relevant objects and values while the tempMeas will read the temperature of the
# MCU.
#
# File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/class_file.py
# @author Tatum Yee and Tyler J. Guffey
# @date 9 February 2021

## Initializes, measures and returns the nucleo's internal temperature sensor 
#  reading
class mcu_temp:
    '''
    @brief This module will activate the mcu and set up some commands
    @details This class sets up the nucelo's temperature sensor and uses it to return its
          current internal temperature
    '''
    ## Initializes nucleo's temperature sensor
    def __init__(self, temp_sensor):
        '''
        @brief Creates a MCU object 
        @param temp_sensor  An object representing the nucleo's temperature sensor    
        '''   
        ## Temperature sensor on nucleo used to measure internal board temp
        self.temp_sensor = temp_sensor
    
    ## Returns nucelo's internal temperature in deg Celsius 
    def tempMeas(self):
        '''
        @brief Measures internal board temperature
        '''
        
        self.temp_sensor.read_core_vref()                 
        ## val represents the nucleo's internal temperature read by the sensor
        val = self.temp_sensor.read_core_temp()           
        return(val)
        #print(val)
        
        