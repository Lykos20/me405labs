## @file mcp_file.py
# This file creates a class for the MCP9808 digital temperature sensor.
#
# The four functions for this class are init, check, temp_celsius, and temp_fahrenheit. 
# Init will initialize all relevant objects, check will verify that the manufacturing
# ID read matches the manufacturing ID provided by the MCP9808 datasheet, temp_celsius
# returns the temperature read in degrees celsius, and temp_fahrenheit returns
# the temperature read in degrees fahrenheit.
#
# File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/mcp_file.py
# @author Tatum X. Yee and Tyler J. Guffey
# @date 13 February 2021

from pyb import I2C
import utime

## Class that reads temperature from MCP9808 temperature module
class mcp:
    '''
    @brief Initializes i2c and address to be used for checking connection and
           returning a temperature value
    '''
    ## Initialize the mcp module
    def __init__(self, i2c, address):
        '''
        @brief Creates a MCP object 
        @param i2c  An object representing the i2c module of the mcp9808
        @param address An object representing the address of the mcp module
        ''' 
        
        ## The i2c object to use for the mcp9808 identification
        self.i2c = i2c
        ## The address of the mcp9808 on the Nucleo
        self.address = address
        
        #data = i2c.mem_read(3, address, temp_reg)
    
    ## Define a function to check if the mcp9808 is actually the i2c object that is connected
    def check(self, i2c):
        ##The address obtained by looking for the mcp9808 sensor
        addie = i2c.scan()
        
        ## If the sensor address is 24, we have the mcp9808 connected
        if addie[0] == 24:
            print('MCP9808 is connected')
            #print(addie)
        
        ## If the address is any other value, we know the sensor connected is incorrect
        else:
            print('Bug off and find the right sensor before using this module')
            #print(addie)
    
    ## A function to obtain the temperature from the mcp in Celsius
    def temp_celsius(self, val):
        #val = self.i2c.mem_read(3, self.address, self.temp_reg)
        value = (val[0] <<8) | val[1]
        
        ## The temperature give or take
        self.temp = (value & 0xFFF) / 16.0
        if value & 0x1000:
            self.temp -= 256.00
            
        ## Return the temperature after processing
        return(self.temp)
        #print(str(self.temp) + ' deg Celsius')
    
    ## A function to obtain the temperature from the mcp in Fahrenheit
    def temp_fahrenheit(self, val):
        #val = self.i2c.mem_read(3, self.address, self.temp_reg)
        value = (val[0] <<8) | val[1]
        ## The temperature give or take
        self.temp = (value & 0xFFF) / 16.0
        if value & 0x1000:
            self.temp -= 256.00
            
        ## Adjust from Celsius to Fahrenheit
        self.temp = (self.temp*9/5) + 32
        return(self.temp)
        #print(str(self.temp) + ' deg Fahrenheit')

## A section used to test the mcp module        
if __name__ == '__main__':
    i2c = I2C(1, I2C.MASTER)
    ## Adress of i2c
    address = 24
    ## Temperature register of i2c
    temp_reg = 5
    ## Calling mcp function
    i2c_temp_sensor = mcp(i2c, address)
    i2c_temp_sensor.check(i2c)
    while True:
        try:
            ## Reads mcp data
            val = i2c.mem_read(3, address, temp_reg)
            i2c_temp_sensor.temp_celsius(val)
            i2c_temp_sensor.temp_fahrenheit(val)
            utime.sleep(1)
        except KeyboardInterrupt:
            print('Control-C has been pressed and data collection has ended')
            break
            
    